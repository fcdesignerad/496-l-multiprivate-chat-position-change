<!-- END PAGE HEADER-->
<style>
 .registered_user_table td.nowrap{
    white-space: nowrap;
    width: 1%;
         font-weight:bold;
} 
 .registered_user_table td.nowrap a{
 margin-right:0
 }
.registered_user_table td.see_btn{
     white-space: nowrap;
    width: 1%;
}
.registered_user_table td.see_btn a.btn {
     margin-right:0px;
}
</style>
<?php 
$invalidMsg = false;
if($FormSingleData['is_masked'] == 1 && $userSingleData->registerFrom != "ICH"){
  $invalidMsg = true;
}
?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">

                    <span class="caption-subject bold uppercase"><?php echo __(created_rooms); ?> </span> &nbsp;&nbsp;
                </div>
            </div>


            <div class="portlet-body">
                <div class="portlet light bordered">
                    <div class="portlet-title">
						
					<?php 
						$session = $this->request->session();
					$language='';
					if($session->read('language') == "en_US"){
					  $language = "english";
					}else if($session->read('language') == "en_SP"){
					  $language = "spanish";
					}else{
					  $language = "portuguese";
					} ?>
					
                       
						
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body form_listing">
                        <table class="table table-striped table-bordered table-hover dataTable" width="100%" id="webRegistrationRoomListing">
                           <thead>
                                                   <tr>
                                                       <th><?php echo __(room_name); ?></th>
                                                      
                                                       <th><?php echo __(ManagerName); ?></th>
                                                       <th><?php echo __(created); ?></th>
                                                       <th><?php echo __(users); ?></th>
                                                       <th><?php echo __(Form); ?></th>
                                                       <th><?php echo __(golive); ?></th>
                                                       <th><?php echo __(action); ?></th>
                                                       
                                                   </tr>
                                               </thead>
                                               <tbody>
                                               <?php
                                               if(!empty($webUser)){
                                               foreach ($webUser as $room): ?>
                                               <tr>
                                                   <td><?php echo  $room->roomName; ?></td>
                                                   
                                                   <td><?= $userSingleData->name ?></td>
                                                   <td><?php echo  date('d-m-Y', strtotime( $room->created_datetime ) ) ?></td>
                                                   <td><a  href='<?php echo HTTP_ROOT."webregistration/view-users-room/".$applicationData->id."/".$room->id;?>' class='label label-success'><?php echo __(view_user); ?></a></td>

                                                    <td><a  href='<?php echo HTTP_ROOT."webregistration/edit-form-room/".$applicationData->id."/".$room->id;?>' class='label label-success'><?php echo __(view_form); ?></a></td>

                                                    <td><a  href='<?php echo HTTP_ROOT."multiprivategolive/multiprivateGoliveRoom/".$applicationData->id."/".$room->id;?>' class='label label-success'><?php echo __(view_golive); ?></a></td>
                                                    
                                                    <td>
                                                    <a href="<?php echo HTTP_ROOT.'webregistration/edit-room/'.$applicationData->id.'/'.$room->id;?>" ><i class="fa fa-edit font-dark"></i></a>
                                                        <a href="<?php echo HTTP_ROOT.'webregistration/delete-room/'.$applicationData->id.'/'.$room->id;?>" onclick="return confirm('Do you want to delete?')" class=""><i class="fa fa-trash-o text-danger"></i></a>
                                                    </td>

                                                  
                                               </tr>
                                               <?php endforeach;
                                               } ?>

                                               </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
<!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<script>
$(document).ready(function(){

  <?php if($invalidMsg){ ?>
    $( window ).on( "load", readyFn );
  <?php } ?>

});

function readyFn(){
    $(".row").css("-webkit-filter", "brightness(90%)");
    $(".row").css("pointer-events", "none");
}
</script>