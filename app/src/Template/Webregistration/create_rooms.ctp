<style>
label { padding-right:10px;}

label input {margin-right: 7px;
    margin-top: 8px;}
.ct{
display:none;
}
#small_text{
    margin-top: 20px;
}
</style>

<?php 
//for masking
$invalidMsg = false;
if($FormSingleData['is_masked'] == 1 && $user_type != "ICH"){
  $invalidMsg = true;
}
?>
<script>
var check_source_name = <?php echo !empty(@$formInfo->wowza_source_name) ? "true": "false"; ?>;
</script>
<div class="row">
   <div class="row web_reg_form">
	 <div class="col-md-12 col-sm-12 col-xs-12">
		    <div class="x_panel">
			<div class="caption font-dark">

                    <span class="caption-subject bold uppercase"><?php echo __(room); ?> <?php echo __(Form); ?> </span> &nbsp;&nbsp;
                </div>
               
				<div class="x_content">
					<?php echo $this->Form->create('$basicInfo', [
						'url' => ['controller' => 'Webregistration', 'action' => 'createRooms/'.$applicationId],
						'class'=>'form-horizontal form-label-left',
						'id'=>'createRooms',
						'enctype'=>'multipart/form-data',
						'novalidate'=>'novalidate',
						 'autocomplete'=>'off',

					]);?>

						<div class="row" style="margin-top:10px;margin-bottom:10px;">
						
					   </div>

                    <div class="col-md-12"><?= __(room_info) .' '. __(english) ?>: </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(room_name); ?><span class="required">*</span>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                                <?php echo $this->Form->input('roomName',
                                [
                                'label'=>false,
                                'class'=>'form-control col-md-7 col-xs-12',
                                'id'   => 'roomName',
                                'type' => 'text',
                                'value'=>'']);?>
                                <span id="form_error1" class="help-block help-block-error"></span>
                        </div>
                    </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(room_description); ?>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                                <?php echo $this->Form->input('roomDescription',
                                [
                                'label'=>false,
                                'class'=>'form-control col-md-7 col-xs-12',
                                'id'   => 'roomDescription',
                                'type' => 'textarea',
                                'value'=>'']);?>
                                <span class="help-block help-block-error"></span>
                        </div>
                    </div>
                    
                    <hr/>

                    <div class="col-md-12"><?= __(room_info) .' '. __(portuguese) ?>: </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(room_name); ?>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                                <?php echo $this->Form->input('roomName_pt',
                                [
                                'label'=>false,
                                'class'=>'form-control col-md-7 col-xs-12',
                                'id'   => 'roomName_pt',
                                'type' => 'text',
                                'value'=>'']);?>
                                <span class="help-block help-block-error"></span>
                        </div>
                    </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(room_description); ?>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                                <?php echo $this->Form->input('roomDescription_pt',
                                [
                                'label'=>false,
                                'class'=>'form-control col-md-7 col-xs-12',
                                'id'   => 'roomDescription_pt',
                                'type' => 'textarea',
                                'value'=>'']);?>
                                <span class="help-block help-block-error"></span>
                        </div>
                    </div>

                    <hr/>

                    <div class="col-md-12"><?= __(room_info) .' '. __(spanish) ?>: </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(room_name); ?>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                                <?php echo $this->Form->input('roomName_sp',
                                [
                                'label'=>false,
                                'class'=>'form-control col-md-7 col-xs-12',
                                'id'   => 'roomName_sp',
                                'type' => 'text',
                                'value'=>'']);?>
                                <span class="help-block help-block-error"></span>
                        </div>
                    </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(room_description); ?>
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                                <?php echo $this->Form->input('roomDescription_sp',
                                [
                                'label'=>false,
                                'class'=>'form-control col-md-7 col-xs-12',
                                'id'   => 'roomDescription_sp',
                                'type' => 'textarea',
                                'value'=>'']);?>
                                <span class="help-block help-block-error"></span>
                        </div>
                    </div>

					<hr/>
					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-md-offset-3">
							
							<button id="send" type="submit" class="btn btn-success"><?php echo __(submit); ?></button>
						</div>
					</div>
                    <?php echo $this->form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

     <?php if($invalidMsg){ ?>
     $( window ).on( "load", readyFn );
    <?php } ?>

    function readyFn(){
        $(".row").css("-webkit-filter", "brightness(90%)");
        $(".row").css("pointer-events", "none");
    }

    var field_required = "<?php echo __(field_required); ?>";
   
      $('#send').click(function () {
        $("#form_error1").html("");
       

        var roomName = $("#roomName").val();
        if(roomName == ''){
            
         $("#form_error1").html(field_required);    
         $('#roomName').addClass("redborder");
         return false;
        }else{
         $('#roomName').removeClass("redborder");
         $("#form_error1").html("");
        }

        $('#createRooms').submit();
    });

});
</script>