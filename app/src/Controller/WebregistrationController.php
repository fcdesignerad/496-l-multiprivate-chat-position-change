<?php

namespace App\Controller;
use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;
use Cake\Network\Email\Email;
use Cake\Event\Event;
use Cake\Validation\Validator;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Aws\S3\S3Client;
use Cake\I18n\Time;
//static Cake\Core\Configure::write($key, $value);
/**
* Static content controller
*
* This controller will render views from Template/Pages/
*
* @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
*/
class WebregistrationController extends AppController
{
    public $helpers = ['Form'];
    public $paginate = [
         'limit' => 25,
         'order' => [
         'Userss.email' => 'asc'
         ]];
   //$this->loadComponent('Resize');
   //Function for check admin session
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }
    public function initialize()
    {
		parent::initialize();
        $this->loadComponent('Paginator');
	}

	//function to load web registration user listing
	function formlisting()
	{
        $this->viewBuilder()->layout('admin_inner');
        $userId = $this->Auth->user('id');
        $userRole = $this->Auth->user('role');
        $userParent = $this->Auth->user('parent_id');
        $userSegment = $this->Auth->user('segment_id');
        $SegmentModel = TableRegistry::get('Segments');
        $SegmentData = $SegmentModel->find('all')->where(['id'=>$userSegment])->first();
        $userSegmentId=0;
        if(!empty($SegmentData)){
            $userSegmentId = $SegmentData->segment_admin;
        }
        $usersModel = TableRegistry::get("Users");
        $usersList  = $usersModel->find('list', [
            'keyField' => 'id',
            'valueField' => 'id'
        ])->where(['segment_id' =>$userSegment,'role !=' =>5])->toArray();
        $usersList[$userId] = $this->Auth->user('id');
        $applicationModel = TableRegistry::get("applications");
        if($userRole == "5"){
            if($userId==$userSegmentId){
                $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['Users.id IN'=> $usersList,'id_web_registration_enabled'=>true])->group('group_id')->toArray();
            }else{
                $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['Users.id' =>$userId,'id_web_registration_enabled'=>true])->group('group_id')->toArray();
            }
        }else{
            $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['id_web_registration_enabled'=>true])->group('group_id')->toArray();
            //$applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['id_web_registration_enabled'=>true])->toArray();
        }
        $segmentModel = TableRegistry::get("segments");
        $segments  = $segmentModel->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])->where(['status' => 1])->toArray();
       // print_r($segments);die;
        $this->set(compact('applications','segments'));
        $this->set('_serialize', ['applications']);
        $this->set('page','landing_page');
	}
	function userlisting()
	{
        $this->viewBuilder()->layout('admin_inner');
        $userId = $this->Auth->user('id');
        $userRole = $this->Auth->user('role');
        $userParent = $this->Auth->user('parent_id');
        $userSegment = $this->Auth->user('segment_id');
        $SegmentModel = TableRegistry::get('Segments');
        $SegmentData = $SegmentModel->find('all')->where(['id'=>$userSegment])->first();
        $userSegmentId=0;
        if(!empty($SegmentData)){
            $userSegmentId = $SegmentData->segment_admin;
        }
        $usersModel = TableRegistry::get("Users");
        $usersList  = $usersModel->find('list', [
            'keyField' => 'id',
            'valueField' => 'id'
        ])->where(['segment_id' =>$userSegment,'role !=' =>5])->toArray();
        $usersList[$userId] = $this->Auth->user('id');
        $applicationModel = TableRegistry::get("applications");
        if($userRole == "5"){
            if($userId==$userSegmentId){
                $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['Users.id IN'=> $usersList,'id_web_registration_enabled'=>true])->group('group_id')->toArray();
            }else{
                $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['Users.id' =>$userId,'id_web_registration_enabled'=>true])->group('group_id')->toArray();
            }
        }else{
            //$applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['id_web_registration_enabled'=>true])->group('group_id')->toArray();
            $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['id_web_registration_enabled'=>true])->toArray();
        }
        $segmentModel = TableRegistry::get("segments");
        $segments  = $segmentModel->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])->where(['status' => 1])->toArray();
        // print_r($segments);die;
        $this->set(compact('applications','segments'));
        $this->set('_serialize', ['applications']);
        $this->set('page','regis_users');
	}

	//function to load view for form
    public function editForm($applicationId=null, $roomId)
    {
        $userId = $this->Auth->user('id');

        $roomModel = TableRegistry::get("rooms");
        $roomData  = $roomModel->find('all')->where(['application_id'=>$applicationId, 'id' => $roomId])->first();


        $this->viewBuilder()->layout('admin_inner');
        $WebRegistrationModel = TableRegistry::get("tbl_webregistration_form");
        $applicationModel = TableRegistry::get("applications");
        $this->set('page','landing_page');
        $this->set('editor',true);
        $record_found=false;
        if(!empty($applicationId)){
            $applicationData=$applicationModel->find('all')->where(['id'=>$applicationId,'id_web_registration_enabled'=>true])->first();
            if(!empty($applicationData)){
                $record_found=true;
            }
            $WebRegistrationData=$WebRegistrationModel->find('all')->where(['application_id'=>$applicationId,'roomId' => $roomId,'language'=>$this->siteLanguage()])->first();
            $this->isFormOwner($applicationData);

            // Get active users    
            $AppModel = TableRegistry::get("applications");
            $AppData = $AppModel->find('all')->select(["manager_id"])->where(['id'=> $applicationId])->first();

            $usersModel = TableRegistry::get("Users");
            $user_details = $usersModel->find('all')->where(['id' => $AppData->manager_id])->first();
                
            $userPlansModel = TableRegistry::get("user_plans");  
            $userPlansData = $userPlansModel->find('all')->where(['user_id' =>$AppData->manager_id, 'is_expired'=>0])->first();  

            /* for masking */
            $FormListingModel = TableRegistry::get("form_listings");  
            $FormSingleData=$FormListingModel->find('all')->where(['owner_id' =>$userId])->first();

            $this->set("FormSingleData", $FormSingleData); 

            $this->set("userPlansData", $userPlansData);
            $this->set("user_type", $user_details->registerFrom);

            $this->set('formInfo',$WebRegistrationData);
            $this->set('applicationId',$applicationId);
            $this->set('roomId',$roomId);

            if(!$this->allowRestrictedContent()){
                $this->Flash->error(__(error_no_plan));
            }
        }
        if($record_found==false){
            $this->redirect(['controller' => 'Webregistration', 'action' => 'formlisting']);
        }
        if(isset($this->request->data) && !empty($this->request->data))
        {

           // print_r($applicationData);die;
            $formId =  $this->request->data['form_id'];
            $WebRegistrationData = $WebRegistrationModel->newEntity();
            if (!file_exists(WWW_ROOT . '/img/uploads/webregimg/logo/')) {
                //$oldMask = umask(0);
                mkdir(WWW_ROOT . '/img/uploads/webregimg/logo/', 0777, true);
                chmod(WWW_ROOT . '/img/uploads/webregimg/logo/', 0777);
                //umask($oldMask);
            }
            if (is_dir(WWW_ROOT . '/img/uploads/webregimg/logo/'))
            {

                $logo = $this->request->data['logo'];
                if( !empty( $logo['name'] ) )
                {
                    $form_img1_info 	=  pathinfo( $logo['name'] );
                    $form_img1_ext 		= $form_img1_info['extension'];
                    $form_img1_new_name = time() . rand(1,999) . '.' . $form_img1_ext;
                    if( $form_img1_ext == 'jpg' || $form_img1_ext == 'jpeg' || $form_img1_ext == 'png' || $form_img1_ext == 'JPG' || $form_img1_ext == 'JPEG'  || $form_img1_ext == 'PNG')
                    {
                        if( !$k = move_uploaded_file($logo['tmp_name'], WWW_ROOT . 'img/uploads/webregimg/logo/' . $form_img1_new_name ) )
                        {
                            $this->Flash->error(__(image_updaload_error));
                            $this->redirect(['controller' => 'Webregistration', 'action' => 'edit-form/'.$applicationId.'/'.$roomId]);
                            echo 'logo having error';
                            //die(image_updaload_error);
                        }else {
                            $WebRegistrationData->logo = @$form_img1_new_name;
                        }
                    }
                    else
                    {
                        $this->Flash->error(__(image_updaload_error));
                         $this->redirect(['controller' => 'Webregistration', 'action' => 'edit-form/'.$applicationId.'/'.$roomId]);
                        echo 'Logo file type not allowed';
                        //die(image_updaload_error);
                    }
                }else{
					$img_status1 = $this->request->data['img_status1'];
					if(img_status1==1){
						$WebRegistrationData->logo ='';
					}
                }
            }

            if (!file_exists(WWW_ROOT . '/img/uploads/webregimg/banner/')) {
                //$oldMask = umask(0);
                mkdir(WWW_ROOT . '/img/uploads/webregimg/banner/', 0777, true);
                chmod(WWW_ROOT . '/img/uploads/webregimg/banner/', 0777);
                //umask($oldMask);
            }
            if (is_dir(WWW_ROOT . '/img/uploads/webregimg/banner/')) {
                $bannerimage = $this->request->data['bannerimage'];
                if (!empty($bannerimage['name'])) {
                    $form_img2_info = pathinfo($bannerimage['name']);
                    $form_img2_ext = $form_img2_info['extension'];
                    $form_img2_new_name = time() . rand(1, 999) . '.' . $form_img2_ext;
                    if ($form_img2_ext == 'jpg' || $form_img2_ext == 'jpeg' || $form_img2_ext == 'png' || $form_img2_ext == 'JPG' || $form_img2_ext == 'JPEG' || $form_img2_ext == 'PNG') {
                        if (!move_uploaded_file($bannerimage['tmp_name'], WWW_ROOT . 'img/uploads/webregimg/banner/' . $form_img2_new_name)) {
                            $this->Flash->error(__('Unable to upload Image 2 '));
                            $this->redirect(['controller' => 'Webregistration', 'action' => 'edit-form/'.$applicationId.'/'.$roomId]);
                            echo 'Cover file having error';
                           // die('Error');
                        }
                        $WebRegistrationData->cover_image = @$form_img2_new_name;
                    } else {
                        $this->Flash->error(__(image_updaload_error));
                        $this->redirect(['controller' => 'Webregistration', 'action' => 'edit-form/'.$applicationId.'/'.$roomId]);
                        echo 'Cover file type not allowed';
                        //die(image_updaload_error);
                    }
                }else{
					$img_status2 = $this->request->data['img_status2'];
					if(img_status2==1){
						$WebRegistrationData->cover_image ='';
					}
                }
            }

			

            $WebRegistrationData->description =@$this->request->data['form_description'];
            $WebRegistrationData->title =@$this->request->data['form_title'];
            $WebRegistrationData->application_id =$applicationData->id;
            $WebRegistrationData->manager_id =$applicationData->manager_id;
            $WebRegistrationData->theme_color =@$this->request->data['theme_color'];
            $WebRegistrationData->text_color =@$this->request->data['text_color'];
			
			
			$WebRegistrationData->facebook_url =@$this->request->data['facebook_url'];
			$WebRegistrationData->twitter_url =@$this->request->data['twitter_url'];
			$WebRegistrationData->website_url =@$this->request->data['website_url'];
			$WebRegistrationData->skype_url  =@$this->request->data['skype_url'];
			$WebRegistrationData->linkedIn_url  =@$this->request->data['linkedIn_url'];
            $WebRegistrationData->instagram_url  =@$this->request->data['instagram_url'];
			$WebRegistrationData->youtube_url  =@$this->request->data['youtube_url'];
			$WebRegistrationData->company_email =@$this->request->data['company_email'];
			$WebRegistrationData->company_name =@$this->request->data['company_name'];
			$WebRegistrationData->footer_title =@$this->request->data['footer_title'];
			$WebRegistrationData->footer_sub_title =@$this->request->data['footer_sub_title'];
			
			$WebRegistrationData->language=$this->siteLanguage();

            if($formId){
               // print_r($WebRegistrationData);die;
                $data=['description'=>$WebRegistrationData->description,'title'=>$WebRegistrationData->title,'application_id'=>$WebRegistrationData->application_id,'manager_id'=>$WebRegistrationData->manager_id,'theme_color'=>$WebRegistrationData->theme_color,'text_color'=>$WebRegistrationData->text_color,'facebook_url'=>$WebRegistrationData->facebook_url,'twitter_url'=>$WebRegistrationData->twitter_url,'website_url'=>$WebRegistrationData->website_url,'skype_url'=>$WebRegistrationData->skype_url,'linkedIn_url'=>$WebRegistrationData->linkedIn_url,'instagram_url'=>$WebRegistrationData->instagram_url,'youtube_url'=>$WebRegistrationData->youtube_url,'company_email'=>$WebRegistrationData->company_email,'footer_sub_title'=>$WebRegistrationData->footer_sub_title,'company_name'=>$WebRegistrationData->company_name,'footer_title'=>$WebRegistrationData->footer_title];
                if($WebRegistrationData->cover_image){
                    $data['cover_image']=$WebRegistrationData->cover_image;
                }else{
					$img_status2 = $this->request->data['img_status2'];
					if($img_status2==1){
						$data['cover_image']='';
					}
                }
                if($WebRegistrationData->logo){
                    $data['logo']=$WebRegistrationData->logo;
                }else{
					$img_status1 = $this->request->data['img_status1'];
					if($img_status1==1){
						$data['logo']='';
					}
                }
                $WebRegistrationModel->updateAll($data,['id'=>$formId,'language'=>$WebRegistrationData->language, 'roomId'=>$roomId]);
				$WebRegistrationModel->updateAll(['facebook_url'=>$WebRegistrationData->facebook_url,'twitter_url'=>$WebRegistrationData->twitter_url,'website_url'=>$WebRegistrationData->website_url,'skype_url'=>$WebRegistrationData->skype_url,'linkedIn_url'=>$WebRegistrationData->linkedIn_url,'instagram_url'=>$WebRegistrationData->instagram_url,'youtube_url'=>$WebRegistrationData->youtube_url],['application_id'=>$applicationData->id,'language'=>'English', 'roomId'=>$roomId]);
				$WebRegistrationModel->updateAll(['facebook_url'=>$WebRegistrationData->facebook_url,'twitter_url'=>$WebRegistrationData->twitter_url,'website_url'=>$WebRegistrationData->website_url,'skype_url'=>$WebRegistrationData->skype_url,'linkedIn_url'=>$WebRegistrationData->linkedIn_url,'instagram_url'=>$WebRegistrationData->instagram_url,'youtube_url'=>$WebRegistrationData->youtube_url],['application_id'=>$applicationData->id,'language'=>'Portuguese', 'roomId'=>$roomId]);
				$WebRegistrationModel->updateAll(['facebook_url'=>$WebRegistrationData->facebook_url,'twitter_url'=>$WebRegistrationData->twitter_url,'website_url'=>$WebRegistrationData->website_url,'skype_url'=>$WebRegistrationData->skype_url,'linkedIn_url'=>$WebRegistrationData->linkedIn_url,'instagram_url'=>$WebRegistrationData->instagram_url,'youtube_url'=>$WebRegistrationData->youtube_url],['application_id'=>$applicationData->id,'language'=>'Spanish', 'roomId'=>$roomId]);
				$response=true;
            }else{
               $response= $WebRegistrationModel->save($WebRegistrationData);
            }
            if($response){
                $this->Flash->success(__(updaload_success));
                $this->redirect(['controller' => 'Webregistration', 'action' => 'edit-form/'.$applicationId.'/'.$roomId]);
            }
        }
    }

    //function to load web registration user listing
    function viewUsers($application_id, $roomId)
    {
        $this->viewBuilder()->layout('admin_inner');
        $userId = $this->Auth->user('id');
        $userRole = $this->Auth->user('role');
        $userParent = $this->Auth->user('parent_id');
        $webUserModel = TableRegistry::get("multiprivate_users");
        $webUser  = $webUserModel->find('all')->contain('Users')->where(['application_id'=>$application_id, 'roomId' => 0])->order('created_datetime desc')->toArray();
        // Get active users    
        $AppModel = TableRegistry::get("applications");
        $AppData = $AppModel->find('all')->select(["manager_id"])->where(['id'=> $application_id])->first();

        $usersModel = TableRegistry::get("Users");
        $user_details = $usersModel->find('all')->where(['id' => $AppData->manager_id])->first();
            
        $userPlansModel = TableRegistry::get("user_plans");  
        $userPlansData = $userPlansModel->find('all')->where(['user_id' =>$AppData->manager_id, 'is_expired'=>0])->first();  

        $this->set("userPlansData", $userPlansData); 
        $this->set("user_type", $user_details->registerFrom);

        /*for masking*/
        $FormListingModel = TableRegistry::get("form_listings");  
        $FormSingleData=$FormListingModel->find('all')->where(['owner_id' =>$userId])->first();

        $this->set("FormSingleData", $FormSingleData);

        $this->set(compact('webUser','application_id', 'roomId'));
        $this->set('_serialize', ['webUser']);
        $this->set('page','regis_users');

        if(!$this->allowRestrictedContent()){
            $this->Flash->error(__(error_no_plan));
        }
    }

    //function to load web registration user listing
    function viewMultiprivateUsers()
    {
        $this->viewBuilder()->layout('admin_inner');
        $userId = $this->Auth->user('id');
        $userRole = $this->Auth->user('role');
        $userParent = $this->Auth->user('parent_id');
        $webUserModel = TableRegistry::get("multiprivate_users");
        $webUser  = $webUserModel->find('all')->contain('Users')->order('created_datetime desc')->toArray();
        
        $this->set('webUser', $webUser);
        $this->set('page','multiprivate_users');
    }

    //function to import user
    function importUser(){
        //$filePointer = fopen($this->request->data['import']['tmp_name'], "rb");
        // open the file
        $application_id=$this->request->data['application_id'];
        $room_id=$this->request->data['room_id'];
        $handle = fopen($this->request->data['import']['tmp_name'], "r");

        // read the 1st row as headings
        $header = fgetcsv($handle);

        // create a message container
        $return = array(
            'messages' => array(),
            'errors' => array(),
        );
        $i=0;
        $applicationModel = TableRegistry::get("applications");
        $applicationData=$applicationModel->find('all')->where(['id'=>$application_id,'id_web_registration_enabled'=>true])->first();
        $webUserModel = TableRegistry::get("multiprivate_users");
        // read each data row in the file
        while (($row = fgetcsv($handle)) !== FALSE) {
            $is_email_exists=$webUserModel->find('all')->where(['application_id'=>$application_id,'userEmail'=>$row[2]])->first();
            if(empty($is_email_exists)) {
                $webUserdata = $webUserModel->newEntity();
                $webUserdata->firstName=$name= trim($row[0]);
                $webUserdata->userEmail =$email=trim($row[2]);
                if($row[1]){
                    $webUserdata->lastName=$lastName= trim($row[1]);
                }else{
                    $webUserdata->lastName=$lastName= "";
                }
                $password=trim($row[3]);
                if($password==''){
                    $password=$this->randomPassword();
                }
                $webUserdata->userMobile = trim($row[4]);
                $webUserdata->userPassword = md5($password);
                $webUserdata->application_id = $application_id;
                $webUserdata->manager_id = $applicationData->manager_id;

                $webUserdata->roomId = $room_id;
                $webUserdata->userStatus = 0;
                $webUserdata->userVerification = 1;
                $webUserdata->userPhoneVerification = 1;
                $webUserdata->userType = 1;
                $webUserdata->userSecret = $this->generateRandomString();
                $webUserdata->lastModified = getUtcTime();
                $no = rand(99999,1000000);
                $webUserdata->verification_code = $no;
                if($row[5]){
                    $webUserdata->country_code = "+".$row[5];
                }else{
                    $webUserdata->country_code = "+55";
                }
                

              
                $response=$webUserModel->save($webUserdata);
                if($response){

                    //Create Chat data
                    $user_id = $response->userId;
                    $webUserdt=$webUserModel->find('all')->where(['userId'=>$user_id])->first();
                    if(!empty($webUserdt)){
                        $webUserModel->updateAll(['userStatus'=>1],['userId'=>$user_id]);

                        
                        // Create group
                        $imGroupModel = TableRegistry::get("im_group");
                        $imGrpData=$imGroupModel->find('all')->where(['application_id'=>$application_id, 'roomId' => $room_id])->first();

                        $imGroupMemberModel = TableRegistry::get("im_group_members");

                        if(count($imGrpData) == 1){

                            $dt=$imGroupMemberModel->find('all')->where(['g_id'=>$imGrpData->g_id, 'u_id'=>$user_id])->first();

                            if(empty($dt)){

                                $imGroupMemberdata = $imGroupMemberModel->newEntity();
                                $imGroupMemberdata->g_id= $imGrpData->g_id;
                                $imGroupMemberdata->u_id= $user_id;
                                $imGroupMemberModel->save($imGroupMemberdata);
                            }

                        }else{

                            $imGroupdata = $imGroupModel->newEntity();
                            $imGroupdata->createdBy= $user_id;
                            $imGroupdata->type= 0;
                            $imGroupdata->block= 0;
                            $imGroupdata->lastActive = date('Y-m-d H:i:s');
                            $imGroupdata->application_id = $application_id;
                            $imGroupdata->roomId = $room_id;
                            $response=$imGroupModel->save($imGroupdata);

                            if($response){
                                $imGroupMemberdata = $imGroupMemberModel->newEntity();
                                $imGroupMemberdata->g_id= $response->g_id;
                                $imGroupMemberdata->u_id= $user_id;
                                $imGroupMemberModel->save($imGroupMemberdata);
                            }
                        }
                    
                    }

                    //Create chat data
                   // if ($this->user('role') == 1 || $this->user('role') == 5) {
                        $data = array( 'email' => $email, 'password' => $password, 'subject' => __(Web_registration_mail_subject), 'name' => $name, 'email_heading' => __(Web_registration_mail_subject), 'form_link' => WEB_REGISTRATION_URL.md5($application_id) );
                        $this->send_email_html($data,'web_registration_user_created');
                   // }
                }
            }
        }
        // close the file
        fclose($handle);
        $this->Flash->success(__(updaload_success));
        $this->redirect(['controller' => 'Webregistration', 'action' => 'view-users/'.$application_id.'/'.$room_id]);
    }

    //function to approve user
    function approval($application_id,$user_id,$roomId,$status){
        $webUserModel = TableRegistry::get("multiprivate_users");
        $webUserdata=$webUserModel->find('all')->where(['userId'=>$user_id])->first();
        if(!empty($webUserdata)){
            $webUserModel->updateAll(['userStatus'=>$status],['userId'=>$user_id]);

            if($status == 1){
            // Create group
            $imGroupModel = TableRegistry::get("im_group");
            $imGrpData=$imGroupModel->find('all')->where(['application_id'=>$application_id, 'roomId' => $roomId])->first();

            $imGroupMemberModel = TableRegistry::get("im_group_members");

            if(count($imGrpData) == 1){

                $dt=$imGroupMemberModel->find('all')->where(['g_id'=>$imGrpData->g_id, 'u_id'=>$user_id])->first();

                if(empty($dt)){

                    $imGroupMemberdata = $imGroupMemberModel->newEntity();
                    $imGroupMemberdata->g_id= $imGrpData->g_id;
                    $imGroupMemberdata->u_id= $user_id;
                    $imGroupMemberModel->save($imGroupMemberdata);
                }

            }else{

                $imGroupdata = $imGroupModel->newEntity();
                $imGroupdata->createdBy= $user_id;
                $imGroupdata->type= 0;
                $imGroupdata->block= 0;
                $imGroupdata->lastActive = date('Y-m-d H:i:s');
                $imGroupdata->application_id = $application_id;
                $imGroupdata->roomId = $roomId;
                $response=$imGroupModel->save($imGroupdata);

                if($response){
                    $imGroupMemberdata = $imGroupMemberModel->newEntity();
                    $imGroupMemberdata->g_id= $response->g_id;
                    $imGroupMemberdata->u_id= $user_id;
                    $imGroupMemberModel->save($imGroupMemberdata);
                }
            }
        }
        }
        if($roomId == 0){
            $this->redirect(['controller' => 'Webregistration', 'action' => 'view-users/'.$application_id.'/'.$roomId]);
        }else{
            $this->redirect(['controller' => 'Webregistration', 'action' => 'view-users-room/'.$application_id.'/'.$roomId]);
        }
    }
    //function to delete user
    function delete($application_id,$user_id,$roomId){
        $webUserModel = TableRegistry::get("multiprivate_users");
        $webUserdata=$webUserModel->find('all')->where(['userId'=>$user_id])->first();
        if(!empty($webUserdata)){
            $webUserModel->delete($webUserdata);
        }
        if($roomId == 0){
            $this->redirect(['controller' => 'Webregistration', 'action' => 'view-users/'.$application_id.'/'.$roomId]);
        }else{
            $this->redirect(['controller' => 'Webregistration', 'action' => 'view-users-room/'.$application_id.'/'.$roomId]);
        }
    }
    function isFormOwner($applicationData)
    {
        if ($this->user('role') != 1 && $this->user('role') != 5 && $this->user('role') != 4)
        {
            if ($applicationData->manager_id!=$this->Auth->user('id'))
            {
                $this->Flash->success(__(error_403));
                return $this->redirect(['controller' => 'Webregistration', 'action' => 'formlisting']);
            }
        }
    }

    public function editUserForm($applicationId=null,$user_id=null, $roomId)
    {

        $this->viewBuilder()->layout('admin_inner');
        $WebUserModel = TableRegistry::get("multiprivate_users");
        $this->set('page','webregistration');
        $this->set('editor',true);
        $record_found=false;
        if(!empty($applicationId)){
            $webUserData=$WebUserModel->find('all')->where(['userId'=>$user_id])->first();
            $this->set('id_user_active_checked', '');
            if(!empty($webUserData)){
                $record_found=true;
                /*if($webUserData->is_active){
                    $this->set('id_user_active_checked', 'checked');
                }else{
                    $this->set('id_user_active_checked', '');
                }*/
            }

            $this->set('formInfo',$webUserData);
            $this->set('applicationId',$applicationId);
            $this->set('roomId',$roomId);
            $this->set('user_id',$user_id);
        }
        if($record_found==false){
            if($roomId == 0){
            $this->redirect(['controller' => 'Webregistration', 'action' =>'view-users/'.$applicationId.'/'.$roomId]);
            }else{
                $this->redirect(['controller' => 'Webregistration', 'action' =>'view-users-room/'.$applicationId.'/'.$roomId]);
            }
        }
        if(isset($this->request->data) && !empty($this->request->data))
        {
			//,'is_active'=>isset($this->request->data['is_active'])?1:0
            if($user_id){
                // print_r($WebRegistrationData);die;
                $webUserData=$WebUserModel->find('all')->where(['userId'=>$user_id])->first();

                if($this->request->data['password'] != ''){
                   $pass = md5($this->request->data['password']);
                }else{
                    $pass =  $webUserData->userPassword;
            }
            
                $data=['firstName'=>trim($this->request->data['firstName']), 'lastName'=>trim($this->request->data['lastName']),'userPassword'=> $pass];
                $WebUserModel->updateAll($data,['userId'=>$user_id]);

             $this->Flash->success(__(updaload_success));
             if($roomId == 0){
                 $this->redirect(['controller' => 'Webregistration', 'action' =>'view-users/'.$applicationId.'/'.$roomId]);
             }else{
                $this->redirect(['controller' => 'Webregistration', 'action' =>'view-users-room/'.$applicationId.'/'.$roomId]);
             }

            
            }
             
        }
    }
	
    public function addUserForm($applicationId=null, $roomId)
    {

        $this->viewBuilder()->layout('admin_inner');
        $WebUserModel = TableRegistry::get("multiprivate_users");
        $this->set('page','webregistration');
        $this->set('editor',true);
        $record_found=false;
        if(!empty($applicationId)){
            $this->set('applicationId',$applicationId);
            $this->set('roomId',$roomId);
        }
		
		$applicationModel = TableRegistry::get("applications");
       	$applicationData=$applicationModel->find('all')->where(['id'=>$applicationId,'id_web_registration_enabled'=>true])->first();
        if(isset($this->request->data) && !empty($this->request->data))
        {
            $is_email_exists=$WebUserModel->find('all')->where(['application_id'=>$applicationId, 'roomId'=>$roomId,'userEmail'=>trim($this->request->data['email'])])->first();

            $is_phone_exists=$WebUserModel->find('all')->where(['application_id'=>$applicationId, 'roomId'=>$roomId,'userMobile'=>trim($this->request->data['userMobile'])])->first();

            if(empty($is_phone_exists)) {

            if(empty($is_email_exists)) {
				$webUserdata = $WebUserModel->newEntity();
                $webUserdata->firstName=$name= trim($this->request->data['firstName']);
				$webUserdata->userEmail =$email=trim($this->request->data['email']);
                $webUserdata->userMobile=$userMobile= trim($this->request->data['userMobile']);
                $webUserdata->lastName=$lastName= trim($this->request->data['lastName']);

				$password=trim($this->request->data['password']);
				if($password==''){
					$password=$this->randomPassword();
				}
				$webUserdata->userPassword = md5($password);
				$webUserdata->application_id = $applicationId;
                $webUserdata->roomId = $roomId;
				$webUserdata->manager_id = $applicationData->manager_id;
				   // if ($this->user('role') == 1 || $this->user('role') == 5) {
                $webUserdata->userStatus = 0;
                $webUserdata->userVerification = 0;
                $webUserdata->userPhoneVerification = 0;
                $webUserdata->userType = 1;
                $webUserdata->userSecret = $this->generateRandomString();
                $webUserdata->lastModified = getUtcTime();

                $no = rand(99999,1000000);
                $webUserdata->verification_code = $no;

					//}
				$response=$WebUserModel->save($webUserdata);
				 if($response){

                    //send message

                    $register_url = SMS_API_URL;

                    
                    $to = $userMobile;
                    

                    $smsId = "MAPP_".time();
                    

                    //$msg = "Verification code : ".$no;
                    $sms_link = CHAT_CSV_URL.'verify-phone/'.$response->userId;
                    $msg = __(registered_successfully_sms_msg,[$sms_link]);
                    

                    $datanew = array('sendSmsRequest' => array(
                        'from' => 'Multiplier App', 
                        'to'=> "55".$to, 
                        'msg'=> $msg, 
                        'callbackOption'=> "NONE", 
                        'id'=> $smsId
                    ));

                    $headers = array(
                        "Authorization: Basic bnR2bmV3LnNtc29ubGluZTpBVmFvOVhLc1ZP",
                        "Content-Type:application/json",
                        "Accept: application/json"
                    );

                    $smsResult = $this->postCurl($register_url, json_encode($datanew), $headers);
                    $smsResult = json_decode($smsResult);

                    if($smsResult->sendSmsResponse){
                        $sms_report_model = TableRegistry::get("sms_reports_multiprivate");
                        $SmsReportData = $sms_report_model->newEntity();
                        $SmsReportData->receiver_address = $to;
                        $SmsReportData->trigger_type = 2; // send verification code
                        $SmsReportData->created_date = getUtcTime(); 
                        $SmsReportData->message  = $msg; 
                        $SmsReportData->type  = "sms"; 
                        $SmsReportData->status = "send"; 
                        $SmsReportData->user_id = $response->userId;
                        $SmsReportData->application_id = $applicationId;
                        $SmsReportData->roomId = $roomId;
                        $SmsReportData->statusCode = $smsResult->sendSmsResponse->statusCode; 
                        $SmsReportData->statusDescription = $smsResult->sendSmsResponse->statusDescription;
                        $SmsReportData->detailCode = $smsResult->sendSmsResponse->detailCode;
                        $SmsReportData->detailDescription = $smsResult->sendSmsResponse->detailDescription;
                        $SmsReportData->sms_id = $smsId;
                        
                        $res = $sms_report_model->save( $SmsReportData );

                    }

                    // send message

                    $link1='<a href="'.WEB_REGISTRATION_URL.md5($applicationId).'/'.$roomId.'">'.WEB_REGISTRATION_URL.md5($applicationId).'/'.$roomId.'</a>';
                    $link2='<a href="'.CHAT_CSV_URL.'verify-me/'.$response->userId.'">'.__(here).'</a>';
                

                    $message1 = '<br>'.__(registered_successfully_email_msg,[$link1]).'<br><br>';
                    $message2 = '<br>'.__(registered_successfully_email_msg_subtext,[$link2]).'<br><br>';


                     $data = array( 'email' => $email, 'password' => $password, 'subject' => __(Web_registration_mail_subject), 'name' => $name, 'email_heading' => __(Web_registration_mail_subject), 'message1' => $message1,  'message2' => $message2 );
					 $this->send_email_html($data,'web_registration_user_created');
					 $this->Flash->success(__(updaload_success));

                     if($roomId == 0){
                     $this->redirect(['controller' => 'Webregistration', 'action' =>'view-users/'.$applicationId.'/'.$roomId]);
                    }else{
                    $this->redirect(['controller' => 'Webregistration', 'action' =>'view-users-room/'.$applicationId.'/'.$roomId]);
                    }
				 }
			}else{
					 $this->Flash->error(__(verifyEmailErr));
                     $this->redirect(['controller' => 'Webregistration', 'action' =>'addUserForm/'.$applicationId.'/'.$roomId]);
			}
        }else{
            $this->Flash->error(__(verifyPhoneErr));
            $this->redirect(['controller' => 'Webregistration', 'action' =>'addUserForm/'.$applicationId.'/'.$roomId]);
        }
        }
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function golive($id=null){  
        $this->viewBuilder()->layout('admin_inner');
        if(!empty($id)){   
            $Applications = TableRegistry::get('Applications'); 
            $applicationData = $Applications->find('all')->where(['id' =>$id])->first();
            $FormListingModel = TableRegistry::get("form_listings");    
            $FormSingleData=$FormListingModel->find('all')->where(['owner_id'=>$applicationData['manager_id']])->first();
              $groupsModel = TableRegistry::get("groups");    
            $groupsModelData=$groupsModel->find('all')->where(['owner_id'=>$applicationData['manager_id']])->first();

            // Get active users 
            $usersModel = TableRegistry::get("Users");
            $user_details = $usersModel->find('all')->where(['id' => $applicationData['manager_id']])->first();

            $userPlansModel = TableRegistry::get("user_plans");  
            $userPlansData = $userPlansModel->find('all')->where(['user_id' =>$applicationData['manager_id'], 'is_expired'=>0])->first();  

            $this->set("userPlansData", $userPlansData); 
            $this->set("user_type", $user_details->registerFrom);
            
            $this->set('GroupSingleData',$groupsModelData);
            $this->set('FormSingleData',$FormSingleData);
            $this->set('applicationData',$applicationData);
             $this->set('page','go_live');

            if(!$this->allowRestrictedContent()){
                $this->Flash->error(__(error_no_plan));
            }
        }
    }


    public function deleteChat($id=null, $roomId)
    {
        $this->viewBuilder()->layout('');
        $this->set('application_id',$id);
        $this->set('roomId',$roomId);
        $this->set('page','regis_users');
        if(isset($this->request->data)&& !empty($this->request->data))
        {   
            $multUsersModel = TableRegistry::get("multiprivate_users");
            $multUsersData = $multUsersModel->find('all')->select(['userId'])->where(['application_id' =>$this->request->data('application_id'), 'roomId' =>$this->request->data('roomId')])->toArray(); 
            
            $idArr = array();
            if(count($multUsersData) > 0){
                foreach ($multUsersData as $data) {
                    array_push($idArr,$data->userId);
                    
                }

                $strarr = implode(",",$idArr);
                $conn = ConnectionManager::get('default');
                $query = "DELETE  FROM `im_message` WHERE sender IN(".$strarr.")";
                $conn->execute( $query );
                $this->Flash->success(__(record_deleted));
                if($roomId == 0){
                $this->redirect(['controller' => 'webregistration', 'action' => 'view-users/'.$this->request->data('application_id').'/'.$this->request->data('roomId')]);  
                }else{
                     $this->redirect(['controller' => 'webregistration', 'action' => 'view-users-room/'.$this->request->data('application_id').'/'.$this->request->data('roomId')]);
                } 
        }
            
        }
        $this->render('modal/delete-chat');
    }


    function blockusers($application_id,$user_id,$roomId,$status){
        $webUserModel = TableRegistry::get("multiprivate_users");
        $webUserdata=$webUserModel->find('all')->where(['userId'=>$user_id])->first();
        if(!empty($webUserdata)){
            $webUserModel->updateAll(['blockStatus'=>$status],['userId'=>$user_id]);
            
        }
        if($roomId == 0){
            $this->redirect(['controller' => 'Webregistration', 'action' => 'view-users/'.$application_id.'/'.$roomId]);
        }else{
           $this->redirect(['controller' => 'Webregistration', 'action' => 'view-users-room/'.$application_id.'/'.$roomId]); 
        }
    }


    function blockmultiprivateusers($application_id,$user_id,$status){
        $webUserModel = TableRegistry::get("multiprivate_users");
        $webUserdata=$webUserModel->find('all')->where(['userId'=>$user_id])->first();
        if(!empty($webUserdata)){
            $webUserModel->updateAll(['blockStatus'=>$status],['userId'=>$user_id]);
            
        }
        $this->redirect(['controller' => 'Webregistration', 'action' => 'view-multiprivate-users']);
    }


    function viewChat($application_id, $roomId){
        $this->viewBuilder()->layout('admin_inner');
        
        
        $webUserModel = TableRegistry::get("im_group");
        $webUser  = $webUserModel->find('all')->where(['application_id'=>$application_id, 'roomId'=>$roomId])->first();
        $this->set('webUser', $webUser);
        $this->set('page','regis_users');
        
    }

    function viewChatManager($application_id, $roomId){
        $this->viewBuilder()->layout('admin_inner');
        
        
        $webUserModel = TableRegistry::get("im_group");
        $webUser  = $webUserModel->find('all')->where(['application_id'=>$application_id, 'roomId'=>$roomId])->first();
        $this->set('webUser', $webUser);
        $this->set('roomId', $roomId);
        $this->set('application_id', $application_id);
        $this->set('page','regis_users');
        
    }
	// MOVE THESE METHODS TO THE RELEVANT CONTROLLER!!!!!!!!!!!
	//METHODS FOR THE VIDEO GALLERY

	public function videoGallery($application_id){

        $this->viewBuilder()->setLayout('admin_inner');

        $ApplicationVideoTable = TableRegistry::get('ApplicationVideos');

        $application_videos = $ApplicationVideoTable->find()
            ->select([
                "ApplicationVideos.id",
                "ApplicationVideos.video_name",
                "ApplicationVideos.title",
                "ApplicationVideos.video_thumb",
                "ApplicationVideos.description",
                "video_date_only" => "DATE_FORMAT(ApplicationVideos.video_date, '%Y-%m-%d %H:%i:%s')",
                "video_hours_watched" => "TIME(ApplicationVideos.hours_watched)"
            ])
            ->where(["ApplicationVideos.application_id" => $application_id])
            ->order("ApplicationVideos.video_date DESC")
            ->toArray();

        $total_hours_watched = $ApplicationVideoTable->find()
            ->select(["total_hours_watched" => "SEC_TO_TIME( SUM( TIME_TO_SEC(ApplicationVideos.hours_watched) ) )"])
            ->where(["ApplicationVideos.application_id" => $application_id])
            ->group("ApplicationVideos.application_id")
            ->first();

        // Get active users    
        $AppModel = TableRegistry::get("applications");
        $AppData = $AppModel->find('all')->select(["manager_id"])->where(['id'=> $application_id])->first();
            
        $usersModel = TableRegistry::get("Users");
        $user_details = $usersModel->find('all')->where(['id' => $AppData->manager_id])->first();
            
        $userPlansModel = TableRegistry::get("user_plans");  
        $userPlansData = $userPlansModel->find('all')->where(['user_id' =>$AppData->manager_id, 'is_expired'=>0])->first();  
        
        /*for masking*/ 
        $FormListingModel = TableRegistry::get("form_listings");  
        $FormSingleData=$FormListingModel->find('all')->where(['owner_id' =>$AppData->manager_id])->first();

        $this->set("FormSingleData", $FormSingleData);

        $this->set("userPlansData", $userPlansData);   
        $this->set("user_type", $user_details->registerFrom); 
        $this->set('page','video_gallery');
        $this->set("application_videos", $application_videos);
        $this->set("application_id", $application_id);
        $this->set("total_hours_watched", $total_hours_watched);

        if(!$this->allowRestrictedContent()){
            $this->Flash->error(__(error_no_plan));
        }
}
	
	/**
	 * Add new videos or edit existing ones
	 */
	public function addVideoGallery(){

        if(file_exists($_FILES['file']['tmp_name']) || is_uploaded_file($_FILES['file']['tmp_name'])){

            $filename = str_replace(' ', '',basename($_FILES['file']['name']));
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $filename = time().".".$ext;
            $alphanum = "abcdefghijkmnpqrstuvwxyz23456789";
            $codekey = substr(str_shuffle($alphanum), 0, 8);
            $filename = $codekey.$filename;
            
            /*------------------------------------------*/
            $awsModel = TableRegistry::get("config");
            $awsData = $awsModel->find('all')->where(['type' => S3_KEY_TYPE])->first();
            $s3_region=$awsData->region;

            $s3_bucket='multiplierapp-form-data';
            $config = [
               's3' => [
                   'key' => $awsData->key_value,
                    'secret' => $awsData->secret,
                   'bucket' => $s3_bucket
               ]
            ];

            $s3 = S3Client::factory([
                'credentials' => [
                   'key' => $config['s3']['key'],
                   'secret' => $config['s3']['secret']
                ],
                'region' => $s3_region,
                'version' => 'latest'
            ]);
                
            $obj = $s3->putObject([
                            'Bucket'       => $config['s3']['bucket'],
                            'Key'          => 'user-videos/'.$filename,
                            'SourceFile'   => $_FILES['file']['tmp_name'],
                            'ACL'          => 'public-read',
                            'StorageClass' => 'REDUCED_REDUNDANCY'
                    ]); 


            $uploadPath = S3_MEDIA_PATH_ELECTRONICS_USER_VIDEO;

            $pathinfo = pathinfo($uploadPath.$filename);
            
            $v_name = $pathinfo['filename'];
            $filenameWithPath = $uploadPath.$filename;
            $newfilename_thumb = $v_name.'_thumb.jpg';
            $newFilenameWithPath_thumb = $uploadPath.$newfilename_thumb;

            $video_thumb = 'ffmpeg -i '.$filenameWithPath.' -vframes 1 -an -s 400x222 -ss 3 '.$newFilenameWithPath_thumb;

            exec($video_thumb);


             /*------------------upload thumb of video------------------------*/

            $obj=$s3->putObject([
                            'Bucket'       => $config['s3']['bucket'],
                            'Key'          => 'user-videos/'.$newfilename_thumb,
                            'SourceFile'   => $_FILES['file']['tmp_name'],
                            'ACL'          => 'public-read',
                            'StorageClass' => 'REDUCED_REDUNDANCY'
                        ]);
            /*------------------------------------------*/
        }

        $ApplicationVideoTable = TableRegistry::get('ApplicationVideos');

        if( $_POST['video_editor_id'] ){
            $application_video = $ApplicationVideoTable->get( $_POST['video_editor_id'] );
        }
        else{
            $application_video = $ApplicationVideoTable->newEntity();
            $application_video->created = Time::now();
        }

        $application_video->application_id = $_POST['application_id'];
        $application_video->video_name =  ($application_video->video_name && is_null($filename) ) ? $application_video->video_name : $filename;
        $application_video->video_thumb = ($application_video->video_name && is_null($filename) ) ? $application_video->video_thumb : $newfilename_thumb;
        $application_video->title = $_POST['title'];
        $application_video->video_date = $_POST['date'];
        $application_video->description = $_POST['description'];    

        $result = $ApplicationVideoTable->save($application_video);

        $data = [
            'video_name' => $filename,
            'title' => $_POST['title'],
            'date' => $_POST['date'],
            'description' => $_POST['description'],
            'id' => $result->id,
        ];

            echo json_encode($data);die; 
    }
	
	/**
	 * Delete videos
	 */
	public function deleteVideoGallery($video_id){

		$ApplicationVideoTable = TableRegistry::get('ApplicationVideos');

		$application_video = $ApplicationVideoTable->get($video_id);

		$deleted = false;

		if($application_video){

			$ApplicationVideoTable->delete($application_video);
             $awsModel = TableRegistry::get("config");
            $awsData = $awsModel->find('all')->where(['type' => S3_KEY_TYPE])->first();
			$s3_region=$awsData->region;
            $s3_bucket='multiplierapp-form-data';
            $config = [
               's3' => [
                   'key' => $awsData->key_value,
                    'secret' => $awsData->secret,
                   'bucket' => $s3_bucket
               ]
			];
			
            $s3 = S3Client::factory([
                'credentials' => [
                   'key' => $config['s3']['key'],
                   'secret' => $config['s3']['secret']
                ],
				'region' => $s3_region,
				'version' => 'latest'
            ]);

			$obj = $s3->deleteObject([
				'Bucket'  => $config['s3']['bucket'],
				'Key'     => "user-videos/".$application_video->video_name,
			]);
				
			$deleted = true;
		}

		echo $deleted;
		exit();
	}
	
	/**
	 * 
	 */
	public function loadVideoGallery($application_id, $start, $quantity){

        $ApplicationVideoTable = TableRegistry::get('ApplicationVideos');

        $application_videos = $ApplicationVideoTable->find()
            ->select([
                "ApplicationVideos.id",
                "ApplicationVideos.video_name",
                "ApplicationVideos.video_thumb",
                "ApplicationVideos.title",
                "ApplicationVideos.description",
                "video_date_only" => "DATE_FORMAT(ApplicationVideos.video_date, '%Y-%m-%d %H:%i:%s')",
                "video_hours_watched" => "TIME(ApplicationVideos.hours_watched)"
            ])
            ->where(["ApplicationVideos.application_id" => $application_id])
            ->order("ApplicationVideos.video_date DESC")
            ->offset($start)
            ->limit($quantity)
            ->toArray();
        
        echo json_encode($application_videos);
        die();
    }

	/**
	 * 
	 */
    public function getTotalHoursWatched($application_id){

		$this->viewBuilder()->setLayout('admin_inner');

		$ApplicationVideoTable = TableRegistry::get('ApplicationVideos');

		$total_hours_watched = $ApplicationVideoTable->find()
			->select(["total_hours_watched" => "SEC_TO_TIME( SUM( TIME_TO_SEC(ApplicationVideos.hours_watched) ) )"])
			->where(["ApplicationVideos.application_id" => $application_id])
			->group("ApplicationVideos.application_id")
			->first();

		echo $total_hours_watched;
		die();
	}
	
        //function to create rooms
    function createRooms($applicationId=null){
       $this->viewBuilder()->layout('admin_inner');

        $WebUserModel = TableRegistry::get("rooms");
        $this->set('page','webregistration');
        $this->set('editor',true);
        
        if(!empty($applicationId)){
            $this->set('applicationId',$applicationId);
        }
        
        $applicationModel = TableRegistry::get("applications");
        $applicationData=$applicationModel->find('all')->where(['id'=>$applicationId])->first();

        $FormListingModel = TableRegistry::get("form_listings");    
        $FormSingleData=$FormListingModel->find('all')->where(['owner_id'=>$applicationData['manager_id']])->first();
        /*for masking*/
        $usersModel = TableRegistry::get("Users");
        $user_details = $usersModel->find('all')->where(['id' => $applicationData['manager_id']])->first();
        $this->set("user_type", $user_details->registerFrom);
        $this->set('FormSingleData',$FormSingleData);



        $roomData=$WebUserModel->find('all')->where(['application_id'=>$applicationId])->toArray();

        if(count($roomData) >0){
            $cnt = count($roomData) + 1;
        }else{
            $cnt = 1;
        }

        if(isset($this->request->data) && !empty($this->request->data)){
            
            $webUserdata = $WebUserModel->newEntity();
            $webUserdata->roomName = trim($this->request->data['roomName']);
            $webUserdata->roomDescription = trim($this->request->data['roomDescription']);

            $webUserdata->roomName_pt = trim($this->request->data['roomName_pt']);
            $webUserdata->roomDescription_pt = trim($this->request->data['roomDescription_pt']);

            $webUserdata->roomName_sp = trim($this->request->data['roomName_sp']);
            $webUserdata->roomDescription_sp = trim($this->request->data['roomDescription_sp']);

            $webUserdata->application_id = $applicationId;
            $webUserdata->manager_id = $applicationData->manager_id;
            $webUserdata->stream_name = "private".$FormSingleData['wowza_source_name']."_".$cnt;

                //}
            $response=$WebUserModel->save($webUserdata);
             if($response){

                //Start - create repeater For multiprivate//

                $repeatersModel = TableRegistry::get('repeaters');
                $rep = $repeatersModel->newEntity();
                $rep->application_id = $applicationData->id;
                $rep->room_id = $response->id;
                $reps = $repeatersModel->save($rep);

                $this->multiprivateCreateAllServersRepeaters($reps->id, $applicationData->application_name, $response->stream_name, $applicationData->application_name, $FormSingleData->isRTSPUser);

                //Stop - create repeater For multiprivate//

                $this->saveWebRegistrationByLanguageForRoom($applicationData->manager_id,'English', $response->id);
                $this->saveWebRegistrationByLanguageForRoom($applicationData->manager_id,'Portuguese', $response->id);
                $this->saveWebRegistrationByLanguageForRoom($applicationData->manager_id,'Spanish', $response->id);   
                 
                $this->Flash->success(__(updaload_success));
                $this->redirect(['controller' => 'Webregistration', 'action' =>'create-rooms/'.$applicationId]);
             }
            
      
        }
    }

    public function editRoom($aplication_id, $room_id){
        
        $this->viewBuilder()->layout('admin_inner'); 

        $WebUserModel = TableRegistry::get("rooms");
        $this->set('page','webregistration');
        $this->set('editor',true);
        
        if(!empty($aplication_id)){
            $this->set('applicationId',$aplication_id);
        }

        $room = $WebUserModel->find()->where(['id' => $room_id, 'application_id' => $aplication_id])->first();

        if(isset($this->request->data) && !empty($this->request->data)){
            
            $WebUserModel->patchEntity($room, $this->request->data);

            if( $WebUserModel->save($room) ){

                $this->Flash->success(__(updaload_success));
                $this->redirect(['controller' => 'Webregistration', 'action' =>'roomListing/'. $aplication_id]);
            }
            else{
                $this->Flash->error(__(updaload_error));
            }
            

        }

        $this->set('room', $room);
    }

    function saveWebRegistrationByLanguageForRoom($manager_id,$language, $roomId){
        $AppModel = TableRegistry::get('applications');
        $appDetail = $AppModel->find('all')->where(['manager_id' => $manager_id])->first();
        if($appDetail){
            

            $WebRegistrationModel = TableRegistry::get("tbl_webregistration_form");
            $WebRegistrationData = $WebRegistrationModel->newEntity();
            if($language=='English'){
            $WebRegistrationData->description ="And here you can insert a VIDEO DESCRIPTION if you want";
            $WebRegistrationData->title ='HERE YOU CAN INSERT A VIDEO TITLE IF YOU WANT';
                $WebRegistrationData->footer_title = html_entity_decode('More Information', ENT_COMPAT, 'UTF-8');
                $WebRegistrationData->footer_sub_title = html_entity_decode('Here you can add additional texts', ENT_COMPAT, 'UTF-8');
            }else if($language=='Portuguese'){
                $WebRegistrationData->description =html_entity_decode("E AQUI VOCÊ PODE INSERIR UMA DESCRIÇÃO DE VÍDEO SE QUISER", ENT_COMPAT, 'UTF-8');
                $WebRegistrationData->title =html_entity_decode('AQUI VOCÊ PODE INSERIR UM TÍTULO DE VÍDEO SE QUISER', ENT_COMPAT, 'UTF-8');
                $WebRegistrationData->footer_title =html_entity_decode('Mais Informações', ENT_COMPAT, 'UTF-8');
                $WebRegistrationData->footer_sub_title =html_entity_decode('Aqui você pode colocar textos adicionais', ENT_COMPAT, 'UTF-8');
            }else if($language=='Spanish'){
                $WebRegistrationData->description =html_entity_decode("Y AQUÍ PUEDES INSERTAR UNA DESCRIPCIÓN DE VÍDEO SI QUIERES", ENT_COMPAT, 'UTF-8');
                $WebRegistrationData->title =html_entity_decode('AQUÍ USTED PUEDE INSERTAR UN TÍTULO DE VÍDEO SI QUIERES', ENT_COMPAT, 'UTF-8');
                $WebRegistrationData->footer_title=html_entity_decode('Más información', ENT_COMPAT, 'UTF-8');
                $WebRegistrationData->footer_sub_title=html_entity_decode('Aquí puede colocar textos adicionales', ENT_COMPAT, 'UTF-8');
            }
            $WebRegistrationData->application_id =$appDetail->id;
            $WebRegistrationData->manager_id =$manager_id;
            $WebRegistrationData->theme_color ='';
            $WebRegistrationData->text_color ='';
            $WebRegistrationData->facebook_url ='';
            $WebRegistrationData->twitter_url ='';
            $WebRegistrationData->website_url ='';
            $WebRegistrationData->skype_url  ='';
            $WebRegistrationData->linkedIn_url  ='';
            $WebRegistrationData->instagram_url  ='';
            $WebRegistrationData->youtube_url  ='';
            $WebRegistrationData->company_email ='';
            $WebRegistrationData->company_name ='';
            $WebRegistrationData->language =$language;
            $WebRegistrationData->roomId =$roomId;

            $WebRegistrationModel->save($WebRegistrationData);
        }
    }

  //function to load web registration room listing
    function roomListing($applicationId=null){

       $this->viewBuilder()->layout('admin_inner');

        $WebUserModel = TableRegistry::get("rooms");
        
        $this->set('editor',true);
        
        if(!empty($applicationId)){
            $this->set('applicationId',$applicationId);
        }
        
        $applicationModel = TableRegistry::get("applications");
        $applicationData=$applicationModel->find('all')->where(['id'=>$applicationId])->first();

        $userModel = TableRegistry::get("users"); 
        $userSingleData = $userModel->find('all')->where(['id'=>$applicationData->manager_id])->first();
       
        $webUser  = $WebUserModel->find('all')->where(['application_id'=>$applicationId])->order('id desc')->toArray();

        $FormListingModel = TableRegistry::get("form_listings");  
        $FormSingleData=$FormListingModel->find('all')->where(['owner_id' =>$applicationData->manager_id])->first();

        $this->set("FormSingleData", $FormSingleData);
        
        $this->set('webUser', $webUser);
        $this->set('applicationData', $applicationData);
        $this->set('userSingleData', $userSingleData);
        $this->set('page','room_listing');
        
    }


    //function to load view for form
    public function editFormRoom($applicationId=null, $roomId=null)
    {

        $this->viewBuilder()->layout('admin_inner');
        $WebRegistrationModel = TableRegistry::get("tbl_webregistration_form");
        $applicationModel = TableRegistry::get("applications");
        $this->set('page','room_listing');
        $this->set('editor',true);
        $record_found=false;
        if(!empty($applicationId)){
            $applicationData=$applicationModel->find('all')->where(['id'=>$applicationId,'id_web_registration_enabled'=>true])->first();
            if(!empty($applicationData)){
                $record_found=true;
            }
            $WebRegistrationData=$WebRegistrationModel->find('all')->where(['application_id'=>$applicationId,'roomId' => $roomId ,'language'=>$this->siteLanguage()])->first();
            $this->isFormOwner($applicationData);
            $this->set('formInfo',$WebRegistrationData);
            $this->set('applicationId',$applicationId);
            $this->set('roomId',$roomId);
        }
        if($record_found==false){
            $this->redirect(['controller' => 'Webregistration', 'action' => 'formlisting']);
        }
        if(isset($this->request->data) && !empty($this->request->data))
        {

           // print_r($applicationData);die;
            $formId =  $this->request->data['form_id'];
            $WebRegistrationData = $WebRegistrationModel->newEntity();
            if (!file_exists(WWW_ROOT . '/img/uploads/webregimg/logo/')) {
                //$oldMask = umask(0);
                mkdir(WWW_ROOT . '/img/uploads/webregimg/logo/', 0777, true);
                chmod(WWW_ROOT . '/img/uploads/webregimg/logo/', 0777);
                //umask($oldMask);
            }
            if (is_dir(WWW_ROOT . '/img/uploads/webregimg/logo/'))
            {

                $logo = $this->request->data['logo'];
                if( !empty( $logo['name'] ) )
                {
                    $form_img1_info     =  pathinfo( $logo['name'] );
                    $form_img1_ext      = $form_img1_info['extension'];
                    $form_img1_new_name = time() . rand(1,999) . '.' . $form_img1_ext;
                    if( $form_img1_ext == 'jpg' || $form_img1_ext == 'jpeg' || $form_img1_ext == 'png' || $form_img1_ext == 'JPG' || $form_img1_ext == 'JPEG'  || $form_img1_ext == 'PNG')
                    {
                        if( !$k = move_uploaded_file($logo['tmp_name'], WWW_ROOT . 'img/uploads/webregimg/logo/' . $form_img1_new_name ) )
                        {
                            $this->Flash->error(__(image_updaload_error));
                            $this->redirect(['controller' => 'Webregistration', 'action' => 'edit-form-room/'.$applicationId.'/'.$roomId]);
                            echo 'logo having error';
                            //die(image_updaload_error);
                        }else {
                            $WebRegistrationData->logo = @$form_img1_new_name;
                        }
                    }
                    else
                    {
                        $this->Flash->error(__(image_updaload_error));
                        $this->redirect(['controller' => 'Webregistration', 'action' => 'edit-form-room/'.$applicationId.'/'.$roomId]);
                        echo 'Logo file type not allowed';
                        //die(image_updaload_error);
                    }
                }else{
                    $img_status1 = $this->request->data['img_status1'];
                    if(img_status1==1){
                        $WebRegistrationData->logo ='';
                    }
                }
            }

            if (!file_exists(WWW_ROOT . '/img/uploads/webregimg/banner/')) {
                //$oldMask = umask(0);
                mkdir(WWW_ROOT . '/img/uploads/webregimg/banner/', 0777, true);
                chmod(WWW_ROOT . '/img/uploads/webregimg/banner/', 0777);
                //umask($oldMask);
            }
            if (is_dir(WWW_ROOT . '/img/uploads/webregimg/banner/')) {
                $bannerimage = $this->request->data['bannerimage'];
                if (!empty($bannerimage['name'])) {
                    $form_img2_info = pathinfo($bannerimage['name']);
                    $form_img2_ext = $form_img2_info['extension'];
                    $form_img2_new_name = time() . rand(1, 999) . '.' . $form_img2_ext;
                    if ($form_img2_ext == 'jpg' || $form_img2_ext == 'jpeg' || $form_img2_ext == 'png' || $form_img2_ext == 'JPG' || $form_img2_ext == 'JPEG' || $form_img2_ext == 'PNG') {
                        if (!move_uploaded_file($bannerimage['tmp_name'], WWW_ROOT . 'img/uploads/webregimg/banner/' . $form_img2_new_name)) {
                            $this->Flash->error(__('Unable to upload Image 2 '));
                            $this->redirect(['controller' => 'Webregistration', 'action' => 'edit-form-room/'.$applicationId.'/'.$roomId]);
                            echo 'Cover file having error';
                           // die('Error');
                        }
                        $WebRegistrationData->cover_image = @$form_img2_new_name;
                    } else {
                        $this->Flash->error(__(image_updaload_error));
                        $this->redirect(['controller' => 'Webregistration', 'action' => 'edit-form-room/'.$applicationId.'/'.$roomId]);
                        echo 'Cover file type not allowed';
                        //die(image_updaload_error);
                    }
                }else{
                    $img_status2 = $this->request->data['img_status2'];
                    if(img_status2==1){
                        $WebRegistrationData->cover_image ='';
                    }
                }
            }

            

            $WebRegistrationData->description =@$this->request->data['form_description'];
            $WebRegistrationData->title =@$this->request->data['form_title'];
            $WebRegistrationData->application_id =$applicationData->id;
            $WebRegistrationData->manager_id =$applicationData->manager_id;
            $WebRegistrationData->theme_color =@$this->request->data['theme_color'];
            $WebRegistrationData->text_color =@$this->request->data['text_color'];
            
            
            $WebRegistrationData->facebook_url =@$this->request->data['facebook_url'];
            $WebRegistrationData->twitter_url =@$this->request->data['twitter_url'];
            $WebRegistrationData->website_url =@$this->request->data['website_url'];
            $WebRegistrationData->skype_url  =@$this->request->data['skype_url'];
            $WebRegistrationData->linkedIn_url  =@$this->request->data['linkedIn_url'];
            $WebRegistrationData->instagram_url  =@$this->request->data['instagram_url'];
            $WebRegistrationData->youtube_url  =@$this->request->data['youtube_url'];
            $WebRegistrationData->company_email =@$this->request->data['company_email'];
            $WebRegistrationData->company_name =@$this->request->data['company_name'];
            $WebRegistrationData->footer_title =@$this->request->data['footer_title'];
            $WebRegistrationData->footer_sub_title =@$this->request->data['footer_sub_title'];
            
            $WebRegistrationData->language=$this->siteLanguage();

            if($formId){
               // print_r($WebRegistrationData);die;
                $data=['description'=>$WebRegistrationData->description,'title'=>$WebRegistrationData->title,'application_id'=>$WebRegistrationData->application_id,'manager_id'=>$WebRegistrationData->manager_id,'theme_color'=>$WebRegistrationData->theme_color,'text_color'=>$WebRegistrationData->text_color,'facebook_url'=>$WebRegistrationData->facebook_url,'twitter_url'=>$WebRegistrationData->twitter_url,'website_url'=>$WebRegistrationData->website_url,'skype_url'=>$WebRegistrationData->skype_url,'linkedIn_url'=>$WebRegistrationData->linkedIn_url,'instagram_url'=>$WebRegistrationData->instagram_url,'youtube_url'=>$WebRegistrationData->youtube_url,'company_email'=>$WebRegistrationData->company_email,'footer_sub_title'=>$WebRegistrationData->footer_sub_title,'company_name'=>$WebRegistrationData->company_name,'footer_title'=>$WebRegistrationData->footer_title];
                if($WebRegistrationData->cover_image){
                    $data['cover_image']=$WebRegistrationData->cover_image;
                }else{
                    $img_status2 = $this->request->data['img_status2'];
                    if($img_status2==1){
                        $data['cover_image']='';
                    }
                }
                if($WebRegistrationData->logo){
                    $data['logo']=$WebRegistrationData->logo;
                }else{
                    $img_status1 = $this->request->data['img_status1'];
                    if($img_status1==1){
                        $data['logo']='';
                    }
                }
                $WebRegistrationModel->updateAll($data,['id'=>$formId,'language'=>$WebRegistrationData->language, 'roomId'=>$roomId]);
                $WebRegistrationModel->updateAll(['facebook_url'=>$WebRegistrationData->facebook_url,'twitter_url'=>$WebRegistrationData->twitter_url,'website_url'=>$WebRegistrationData->website_url,'skype_url'=>$WebRegistrationData->skype_url,'linkedIn_url'=>$WebRegistrationData->linkedIn_url,'instagram_url'=>$WebRegistrationData->instagram_url,'youtube_url'=>$WebRegistrationData->youtube_url],['application_id'=>$applicationData->id,'language'=>'English', 'roomId'=>$roomId]);
                $WebRegistrationModel->updateAll(['facebook_url'=>$WebRegistrationData->facebook_url,'twitter_url'=>$WebRegistrationData->twitter_url,'website_url'=>$WebRegistrationData->website_url,'skype_url'=>$WebRegistrationData->skype_url,'linkedIn_url'=>$WebRegistrationData->linkedIn_url,'instagram_url'=>$WebRegistrationData->instagram_url,'youtube_url'=>$WebRegistrationData->youtube_url],['application_id'=>$applicationData->id,'language'=>'Portuguese', 'roomId'=>$roomId]);
                $WebRegistrationModel->updateAll(['facebook_url'=>$WebRegistrationData->facebook_url,'twitter_url'=>$WebRegistrationData->twitter_url,'website_url'=>$WebRegistrationData->website_url,'skype_url'=>$WebRegistrationData->skype_url,'linkedIn_url'=>$WebRegistrationData->linkedIn_url,'instagram_url'=>$WebRegistrationData->instagram_url,'youtube_url'=>$WebRegistrationData->youtube_url],['application_id'=>$applicationData->id,'language'=>'Spanish', 'roomId'=>$roomId]);
                $response=true;
            }else{
               $response= $WebRegistrationModel->save($WebRegistrationData);
            }
            if($response){
                $this->Flash->success(__(updaload_success));
                $this->redirect(['controller' => 'Webregistration', 'action' => 'room-listing/'.$applicationId]);
            }
        }
    }

    //function to load web registration user listing
    function viewUsersRoom($application_id, $roomId)
    {
        $this->viewBuilder()->layout('admin_inner');
        $userId = $this->Auth->user('id');
        $userRole = $this->Auth->user('role');
        $userParent = $this->Auth->user('parent_id');
        $webUserModel = TableRegistry::get("multiprivate_users");
        $webUser  = $webUserModel->find('all')->contain('Users')->where(['application_id'=>$application_id, 'roomId' => $roomId])->order('created_datetime desc')->toArray();
        $this->set(compact('webUser','application_id', 'roomId'));
        $this->set('_serialize', ['webUser']);
        $this->set('page','room_listing');
    }

    public function goliveRoom($id=null, $roomId){  
        $this->viewBuilder()->layout('admin_inner');
        if(!empty($id)){   
            $Applications = TableRegistry::get('Applications'); 
            $applicationData = $Applications->find('all')->where(['id' =>$id])->first();
            $FormListingModel = TableRegistry::get("form_listings");    
            $FormSingleData=$FormListingModel->find('all')->where(['owner_id'=>$applicationData['manager_id']])->first();
              $groupsModel = TableRegistry::get("groups");    
            $groupsModelData=$groupsModel->find('all')->where(['owner_id'=>$applicationData['manager_id']])->first();

            $roomModel = TableRegistry::get('rooms');
            $roomData=$roomModel->find('all')->where(['id'=>$roomId])->first();

            $this->set('roomData',$roomData);
            $this->set('GroupSingleData',$groupsModelData);
            $this->set('FormSingleData',$FormSingleData);
            $this->set('applicationData',$applicationData);
             $this->set('page','webregistration');
        }
    }


    function deleteRoom($application_id, $roomId){
        $roomModel = TableRegistry::get("rooms");
        $roomdata=$roomModel->find('all')->where(['id'=>$roomId])->first();
        if(!empty($roomdata)){
            $roomModel->delete($roomdata);

            // Delete form
            $twfModel = TableRegistry::get('tbl_webregistration_form');
            $twfModel->deleteAll(['roomId' => $roomId, 'application_id' => $application_id],false);

            

            // Delete sms
            $srModel = TableRegistry::get('sms_reports_multiprivate');
            $srModel->deleteAll(['roomId' => $roomId, 'application_id' => $application_id],false);

            // Delete group
            $imgModel = TableRegistry::get('im_group');

            $imgData = $imgModel->find('all')->where(['roomId' => $roomId, 'application_id' => $application_id])->first();

            // Delete group member
            $grpmemModel = TableRegistry::get('im_group_members');

            if(count($imgData) >0){
                $grpmemModel->deleteAll(['g_id' => $imgData->g_id],false);
            }


            $imgModel->deleteAll(['roomId' => $roomId, 'application_id' => $application_id],false);

            // Delete chat
            $multUsersModel = TableRegistry::get("multiprivate_users");
            $multUsersData = $multUsersModel->find('all')->select(['userId'])->where(['application_id' =>$application_id, 'roomId' =>$roomId])->toArray(); 
            
            $idArr = array();
            if(count($multUsersData) > 0){
                foreach ($multUsersData as $data) {
                    array_push($idArr,$data->userId);
                    
                }

                $strarr = implode(",",$idArr);
                $conn = ConnectionManager::get('default');
                $query = "DELETE  FROM `im_message` WHERE sender IN(".$strarr.")";
                $conn->execute( $query );

                // Delete users
            $muModel = TableRegistry::get('multiprivate_users');
            $muModel->deleteAll(['roomId' => $roomId, 'application_id' => $application_id],false);

                $this->Flash->success(__(record_deleted));

                
            }

            $this->redirect(['controller' => 'Webregistration', 'action' => 'room-listing/'.$application_id]);

        }
        
    }
	
     /*
  Method : multiprivateCreateAllServersRepeaters()
  Title : Used to create all(8) repeater
  author : Sandhya Ghatoda
  Created Date : 06-10-2020
  */
  public function multiprivateCreateAllServersRepeaters($rep_id, $appName, $sourceName, $destAppName, $isRTSPUser){
    $repeatersModel = TableRegistry::get('repeaters');
    for($i=1;$i<=8;$i++){
      $targetName = "MasterRepeater".rand(1,999).rand(1,999);
      if($i == 1){
        $server_url = HOST1_IP;
      }else if($i == 2){
        $server_url = HOST2_IP;
      }else if($i == 3){
        $server_url = HOST3_IP;
      }else if($i == 4){
        $server_url = HOST4_IP;
      }else if($i == 5){
        $server_url = HOST5_IP;
      }else if($i == 6){
        $server_url = HOST6_IP;
      }else if($i == 7){
        $server_url = HOST7_IP;
      }else if($i == 8){
        $server_url = HOST8_IP;
      }
      $res = $this->multiprivateCreateAllServersRepeatersApi($server_url, $appName,$targetName , $sourceName, $destAppName, $isRTSPUser);
      if($res){
          
          if($i == 1){
            $repeatersModel->updateAll( ['under1_repeater' =>$targetName], ['id'=>$rep_id]);
          }else if($i == 2){
            $repeatersModel->updateAll( ['under2_repeater' =>$targetName], ['id'=>$rep_id]);
          }else if($i == 3){
            $repeatersModel->updateAll( ['under3_repeater' =>$targetName], ['id'=>$rep_id]);
          }else if($i == 4){
            $repeatersModel->updateAll( ['under4_repeater' =>$targetName], ['id'=>$rep_id]);
          }else if($i == 5){
            $repeatersModel->updateAll( ['under5_repeater' =>$targetName], ['id'=>$rep_id]);
          }else if($i == 6){
            $repeatersModel->updateAll( ['under6_repeater' =>$targetName], ['id'=>$rep_id]);
          }else if($i == 7){
            $repeatersModel->updateAll( ['under7_repeater' =>$targetName], ['id'=>$rep_id]);
          }else if($i == 8){
            $repeatersModel->updateAll( ['under8_repeater' =>$targetName], ['id'=>$rep_id]);
          }
      }
    }  
    
  }

  public function multiprivateCreateAllServersRepeatersApi($server_url, $appName,$targetName, $sourceName, $destAppName, $isRTSPUser){

    $url = MAIN_WOWZA_URL."api/v1/addChainning";
    $targetName = clearTarget($targetName);

    $appLastString = substr($appName, -6);

    // if($isRTSPUser &&  $appLastString == '000001'){
    //   $sourceName = "private".$sourceName.".stream";
    // }

    //echo $appName.'---'.$targetName.'---'.$sourceName.'....'.$destAppName;die;

    $data = array('source_name' => $sourceName, 'target_name' => $targetName, 'app_name' => $appName, 'dest_app_name' => $destAppName, 'host_ip'=> $server_url);
    $response = $this->postCurl($url, json_encode($data));
    $result = json_decode($response);



    $text = PHP_EOL. date("Y-m-d H:i:s")."-> URL -> ".$url." -> Data -> ". print_r($data, true) . " -> Response ->". print_r($result, true);
    $myfile = fopen("../webroot/create_all_server_repeater_multiprivate.txt", "a") or die("Unable to open file!");
    fwrite($myfile, $text);        
    fclose($myfile);


    if ($result->status == "success") {

      $urlNew = MAIN_WOWZA_URL."api/v1/deactivateStreamTarget";
      $dataNew = array('app_name'=> $appName, 'target_name'=> [$targetName]);
      $resultNew = $this->postCurl($urlNew, json_encode($dataNew));
      $resultNew = json_decode($resultNew);
      if($resultNew->status == "success") {
        return true;
      }else{
        return false;
      }
      
    }
    return false;
  }
public function viewBroadcastsPrivate(){
        $this->viewBuilder()->layout('admin_inner');
        $this->loadComponent('Paginator');
        $liveStreamModel = TableRegistry::get("live_streams");
        $CommingUsersModel = TableRegistry::get("comming_users");
        $BroadcastDetailModel = TableRegistry::get("broadcast_details");
        $applicationsModel = TableRegistry::get('Applications');
        $applications = $applicationsModel->find('all')->toArray();

        $userId = $this->Auth->user('id');
        if($this->Auth->user('parent_id') != 0){
            $userId = $this->Auth->user('parent_id');
        }
        $userRole = $this->Auth->user('role');
        $userSegment = $this->Auth->user('segment_id');
        $SegmentModel = TableRegistry::get('Segments');
        $SegmentData = $SegmentModel->find('all')->where(['id'=>$userSegment])->first();
        $userSegmentId=0;
        if(!empty($SegmentData)){
          $userSegmentId = $SegmentData->segment_admin;
        }
        $usersModel = TableRegistry::get("Users");
        $usersList  = $usersModel->find('list', [
        'keyField' => 'id',
        'valueField' => 'id'
        ])->where(['segment_id' =>$userSegment,'role !=' =>5])->toArray();
        $usersList[$userId] = $this->Auth->user('id');
        if($userRole==5){
          if($userId==$userSegmentId){  
            $applications = $applicationsModel->find('all')->contain(['Users','Groups'])->where(['Users.id IN'=> $usersList])->toArray();
          }else{
            $applications = $applicationsModel->find('all')->contain(['Users','Groups'])->where(['Users.id' =>$userId])->toArray();  
          }  
        }else{
          $applications = $applicationsModel->find('all')->toArray();
        }
        //echo "<pre>"; print_r($applications);die;
        $apps[0] = 'All';
        foreach($applications as $key => $value){
            $apps[$value['application_name']] = ucfirst( $value['application_name'] );
        }

        $this->set('applications', $apps);        
        $this->set('startDate', date( "m/1/Y 00:00:00"));
        $this->set('endDate', appTime(date( "m/d/Y H:i:s")));
        $this->set('submited', '0');
        $where_count_join = "stream_status = 2";
        $where_join = "stream_status = 2";
        $where =  array('stream_status' => 2 );
        if(isset($this->request->data)&& !empty($this->request->data)){
            $reportrange = $this->request->data['reportrange'];
            $range = explode(' - ', $reportrange);
            $range1 = str_replace("/", "-",$range[0]);
            $range2 = str_replace("/", "-",$range[1]);
            $start = date( "Y-m-d H:i:s", strtotime(convertToUtc($range1)));
            $end = date( "Y-m-d H:i:s", strtotime(convertToUtc($range2)));            
        
            $where = array_merge($where, array('stream_started >=' => $start, 'stream_started <=' => $end) );

            $where_count_join .= " AND stream_started >= '" .$start. "' AND stream_started <= '" . $end . "'";
            $where_join .= " AND stream_started >= '" .$start. "' AND stream_started <= '" . $end . "'"." AND live_type = 1";

            $this->set('startDate',date( "d/m/Y H:i:s", strtotime($range1) ));
            $this->set('endDate',date( "d/m/Y H:i:s", strtotime($range2) ));
            $this->set('submited', '1');
        }

        $appId = @$this->request->data['appid'];
        if ($appId) {
            $where = array_merge( $where, array('stream_app' => $appId) );
            $where_count_join .= " AND stream_app = " .$appId;
            $where_join .= " AND stream_app = " .$appId;
        }

        $userSegment = $this->Auth->user('segment_id');

        if(!in_array( $this->user('role'), ['1', '2'])) {
            $groupsModel = TableRegistry::get("Groups");
            if($userRole==5){
              if($userId==$userSegmentId && !empty($usersList)){    
                  $groupDataList = $groupsModel->find('list', ['keyField' => 'id','valueField' => 'id'])->where(['owner_id IN'=>$usersList])->toArray();
                  if(!empty($groupDataList)){
                     $implodeGroupList =implode(",",$groupDataList);      
                  $where = array_merge( $where, array('group_id IN' => $groupDataList, 'live_type' => 1 ) );
                  $where_count_join .= " AND live_streams.group_id IN (" .$implodeGroupList.")"." AND live_type = 1";
                  $where_join .= " AND LiveStream.group_id IN (" .$implodeGroupList.")"." AND live_type = 1";
                  }
              }else{
                $groupData = $groupsModel->find('all')->where(['owner_id'=>$userId])->first();
                if(!empty($groupData)){
                    $where = array_merge( $where, array('group_id' => $groupData['id'] , 'live_type' => 1 ) );
                    $where_count_join .= " AND live_streams.group_id = " .$groupData['id']." AND live_type = 1";
                    $where_join .= " AND LiveStream.group_id = " .$groupData['id']." AND live_type = 1";
                }else{
                    $where = array('group_id' => 0, 'live_type' => 1);
                    $where_count_join .= " AND live_streams.group_id = 0"." AND live_type = 1";
                    $where_join .= " AND LiveStream.group_id = 0"." AND live_type = 1";
                }   
              }
            }else{
            $groupData = $groupsModel->find('all')->where(['owner_id'=>$this->getManager()])->first();
            $where = array_merge( $where, array('group_id' => $groupData['id'], 'live_type' => 1 ) );
            $where_count_join .= " AND live_streams.group_id = " .$groupData['id']." AND live_type = 1";
            $where_join .= " AND LiveStream.group_id = " .$groupData['id']." AND live_type = 1";
        }

        }else{
            $where = array_merge( $where, array( 'live_type' => 1 ) );
            $where_count_join .= " AND live_type = 1";
            $where_join .= " AND live_type = 1";  
        }
        
        //echo "<pre>";
        //print_r($where);die;
        

        $conn = ConnectionManager::get('default');
        //$TotalReachedQuery = "SELECT IFNULL(SUM(user.followers),0) as Reached FROM `live_streams` as LiveStream LEFT JOIN `broadcast_details` as detail ON detail.stream_id = LiveStream.id LEFT JOIN `comming_users` as user ON detail.target_id = user.id where " . $where_join." AND detail.status != 'UNPUBLISHED'";
        $TotalBroadcastQuery = "SELECT SUM(TIME_TO_SEC(TIMEDIFF(LiveStream.stream_ends, LiveStream.stream_started))) as diff FROM `live_streams` as LiveStream where " . $where_join;

              $FormListingModel = TableRegistry::get("form_listings");
        $FormListingData = $FormListingModel->find('all')->where(['owner_id'=>$userId])->first();
        $userRole = $this->Auth->user('role');
        if($userRole != 1 && $userRole != 2){
            $this->set('form_id',$FormListingData->form_id);
            $liveBroadDataCount = $liveStreamModel->find('all')->where([$where])->count();
        $broadcastResults = $conn->execute( $TotalBroadcastQuery )->fetch('assoc');
            //$followersResults = $conn->execute( $TotalReachedQuery )->fetch('assoc');
            //$this->set('followersSum', $followersResults['Reached']);
        $this->set('totalBroadCasted', secToStamp($broadcastResults['diff']));
            $this->set('liveBroadDataCount', $liveBroadDataCount);    
        }else{
            $this->set('form_id',"");
            $liveBroadDataCount = $liveStreamModel->find('all')->where([$where])->count();
            $broadcastResults = $conn->execute( $TotalBroadcastQuery )->fetch('assoc');
           
            //$this->set('followersSum', $followersResults);
            $this->set('totalBroadCasted', secToStamp($broadcastResults['diff']));
            $this->set('liveBroadDataCount', $liveBroadDataCount); 
        }

        $query = array();               
        $query['count']  = "select count(*) AS count from (SELECT live_streams.id  FROM live_streams where " . $where_count_join.") as countedtable";

        $query['detail'] = "SELECT
            LiveStream.id, 
            LiveStream.group_id, 
            LiveStream.stream_app,
            LiveStream.stream_started,
            LiveStream.lastUpdates,
            LiveStream.stream_ends,
            LiveStream.startDate,
            LiveStream.Total,
            LiveStream.TotalInteraction,
            LiveStream.Error,
            LiveStream.Success,
            LiveStream.Reached, 
            LiveStream.vlikeSum, 
            LiveStream.shareSum,
            LiveStream.viewSum, 
            LiveStream.commentSum, 
            LiveStream.loveSum, 
            LiveStream.wowSum, 
            LiveStream.angrySum, 
            LiveStream.sadSum, 
            LiveStream.hahaSum,
            LiveStream.Broadcasted,
            LiveStream.fb_td_status
            FROM  live_streams as LiveStream
            where " . $where_join;
        $this->request->session()->write('query', $query);


        $this->set('page','webregistration');
        
        if(!$this->allowRestrictedContent()){
            $this->Flash->error(__(error_no_plan));
        }
    }


    public function viewBroadcastsListPrivate(){
        $this->autoRender = false;
        $requestData= $this->request->data;

        extract($this->request->session()->read('query'));

        if($this->Auth->user('role') == 1 || $this->Auth->user('role') == 2){
            $columns = array(
            0 => 'id',
            1 => 'startDate',
            2 => '',
            3 => '',
            4 => 'Broadcasted',
            5 => '',
            6 => 'Appname',
            7 => 'viewers'
            
        );
        }
        else{
        $columns = array(
            0 => 'id',
            1 => 'startDate',
            2 => '',
            3 => '',
            4 => 'Broadcasted',
            5 => '',
            6 => 'viewers'
            
        );
    }
        $searchColumns = array('LiveStream.id', 'LiveStream.stream_started', 'LiveStream.stream_ends');

        $conn = ConnectionManager::get('default');
        $results = $conn->execute($count)->fetchAll('assoc');        
        $totalData = isset($results[0]['count']) ? $results[0]['count'] : 0;
        $totalFiltered = $totalData;

        $sidx = $columns[$requestData['order'][0]['column']];
        $sord = $requestData['order'][0]['dir'];

        $search = $requestData['search']['value'];
        
        $where_search = "";
        if($search){
            $i = 0;

            foreach ($searchColumns as $value) {
                if($value){
                    if($i == 0){
                        $where_search .= " AND ( " . $value . " LIKE '%".$search."%'";
                    }else{
                        $where_search .= " OR " . $value . " LIKE '%".$search."%'";
                    }
                    if($i == count($searchColumns) - 1){
                        $where_search .= ")";
                    }
                    $i++;
                }
            }            
        }
        
        $start = $requestData['start'];
        $length = $requestData['length'];

        $SQL = $detail.$where_search." GROUP BY LiveStream.id ORDER BY $sidx $sord ";

        if($length > -1){
            $SQL .= "LIMIT $start , $length ";
        }

        $results = $conn->execute( $SQL )->fetchAll('assoc');
        $i = 0;
        $data = array();
        foreach ( $results as $row){

            $nestedData= [];
            $nestedData[] = $row["id"];
            //$nestedData[] = $totalData;
            $nestedData[] = ($row["startDate"] != "") ? appDate($row["startDate"]) : "";
            $nestedData[] = appTime($row["stream_started"]);
            $nestedData[] = appTime($row['stream_ends']);
            $nestedData[] = $row['Broadcasted'];
            if ($row['fb_td_status'] == 1){
                $nestedData[] = "<a href=".HTTP_ROOT."commingusers/fb-title/".$row["id"]." data-target='#myModal' data-toggle='ajaxModal'>".__(Read)."</a>";
            }else{
                $nestedData[] = __(Many);
            }


            if($this->Auth->user('role') == 1 || $this->Auth->user('role') == 2){
                $nestedData[] = "<a href=".HTTP_ROOT."formbuilder/listing/".$row["group_id"].">".$row["stream_app"]."</a>";
            }

            $nestedData[] = 0;
            
            $data[] = $nestedData;
            $i++;
        }
        $json_data = array(
            "draw"            => intval( $requestData['draw'] ),
            "recordsTotal"    => intval( $totalData ),
            "recordsFiltered" => intval( $totalFiltered ),
            "data"            => $data
        );
        echo json_encode($json_data);exit;
    }	
	
}